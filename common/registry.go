package cloud

import (
	"bitbucket.org/coldze/go-sample/basic"
	"errors"
)

const (
	CLOUD_COCAINE_11 int = 1
	CLOUD_COCAINE_12 int = 2
)

type Cloud interface {
	GetApp(appName string, args ...interface{}) (basic.Application, error)
	GetID() int
}

type Registry interface {
	Register(methodName string, requestHandler basic.HandlingFunction)
}

type CloudRegistry struct {
	clouds map[int]Cloud
}

type HandlersRegistration func(registry Registry) error

func (registry *CloudRegistry) GetApp(platform int, appName string, args ...interface{}) (basic.Application, error) {
	if registry.clouds == nil {
		registry.clouds = make(map[int]Cloud)
	}
	cloud := registry.clouds[platform]
	if cloud == nil {
		return nil, errors.New("Unregistered platform specified.")
	}
	return cloud.GetApp(appName, args...)
}

func (registry *CloudRegistry) RegisterCloud(cloud Cloud) {
	if registry.clouds == nil {
		registry.clouds = make(map[int]Cloud)
	}
	registry.clouds[cloud.GetID()] = cloud
}
