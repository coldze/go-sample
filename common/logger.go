package cloud

import (
	"bitbucket.org/coldze/go-sample/basic"
)

type wrappedLogger struct {
	prefix string
	logger basic.Logger
}

func (c *wrappedLogger) Debugf(msg string, args ...interface{}) {
	c.logger.Debugf(c.prefix+msg, args...)
}

func (c *wrappedLogger) Infof(msg string, args ...interface{}) {
	c.logger.Infof(c.prefix+msg, args...)
}

func (c *wrappedLogger) Warnf(msg string, args ...interface{}) {
	c.logger.Warnf(c.prefix+msg, args...)
}

func (c *wrappedLogger) Errf(msg string, args ...interface{}) {
	c.logger.Errf(c.prefix+msg, args...)
}

func (c *wrappedLogger) Close() {
	c.logger.Close()
}

func NewPrefixedLogger(prefix string, baseLogger basic.Logger) basic.Logger {
	return &wrappedLogger{prefix: prefix, logger: baseLogger}
}
