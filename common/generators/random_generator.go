package generators

import (
	"math/rand"
	"time"
)

var alphaNumericRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
var numericRunes = []rune("0123456789")

type RandomGenerator func() string

func newRandomGenerator(generationLength int, symbols []rune) RandomGenerator {
	rand.Seed(time.Now().UnixNano())
	symbolsLength := len(symbols)
	return func() string {
		b := make([]rune, generationLength)
		for i := range b {
			b[i] = symbols[rand.Intn(symbolsLength)]
		}
		return string(b)
	}
}

func NewAlphaNumericRandomGenerator(generationLength int) RandomGenerator {
	return newRandomGenerator(generationLength, alphaNumericRunes)
}

func NewNumericRandomGenerator(generationLength int) RandomGenerator {
	return newRandomGenerator(generationLength, numericRunes)
}
