package cloud

import (
	"bitbucket.org/coldze/go-sample/basic"
	"fmt"
)

func NewOnPingHandler(appName string) basic.HandlingFunction {
	replyMessage := fmt.Sprintf("%s: ping reply", appName)
	return func(request []byte, logger basic.Logger) (interface{}, basic.CommonError) {
		return replyMessage, nil
	}
}
