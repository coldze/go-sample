package cocaine12

import (
	"bitbucket.org/coldze/go-sample/basic"
	"bitbucket.org/coldze/go-sample/common"
	"fmt"
	"github.com/cocaine/cocaine-framework-go/cocaine12"
	"github.com/cocaine/cocaine-framework-go/vendor/src/github.com/ugorji/go/codec"
	"golang.org/x/net/context"
	"time"
)

var (
	mh codec.MsgpackHandle
	h  = &mh
)

func NewHandler(methodName string, handler basic.HandlingFunction, baseLogger basic.Logger, errorsFactory basic.ErrorsFactory) func(context.Context, cocaine12.Request, cocaine12.Response) {
	logger := cloud.NewPrefixedLogger(fmt.Sprintf("[%s]: ", methodName), baseLogger)
	return func(ctx context.Context, request cocaine12.Request, response cocaine12.Response) {
		defer response.Close()
		logger.Debugf("Method started.")
		methodStarted := time.Now()
		ctx, done := cocaine12.NewSpan(ctx, methodName)
		defer done()

		body, err := request.Read(ctx)
		if err != nil {
			logger.Errf("Failed to read request. Error: %v.", err)
			internalErr := basic.NewInternalError()
			response.ErrorMsg(internalErr.GetCode(), internalErr.GetMessage())
			return
		}
		defer func() {
			if r := recover(); r != nil {
				logger.Errf("Recovered after panic. Error: %v.", r)
				internalErr := basic.NewInternalError()
				response.ErrorMsg(internalErr.GetCode(), internalErr.GetMessage())
			}
			logger.Infof("Method is done. Time taken nanoseconds: %s", time.Since(methodStarted))
		}()

		result, handlingErr := handler(body, logger)
		if handlingErr != nil {
			logger.Errf("Handling failed. Error: %v. Code: %d", handlingErr.GetMessage(), handlingErr.GetCode())
			response.ErrorMsg(handlingErr.GetCode(), handlingErr.GetMessage())
			return
		}
		var data []byte
		codec.NewEncoderBytes(&data, h).Encode(&result)
		response.Write(data)
	}
}
