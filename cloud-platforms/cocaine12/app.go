package cocaine12

import (
	"bitbucket.org/coldze/go-sample/basic"
	"bitbucket.org/coldze/go-sample/common"
	"errors"
	"fmt"
	cocaineFramework "github.com/cocaine/cocaine-framework-go/cocaine12"
	"github.com/cocaine/cocaine-framework-go/vendor/src/github.com/ugorji/go/codec"
	"golang.org/x/net/context"
	"sync"
	"time"
)

type RegistryImpl struct {
	worker        *cocaineFramework.Worker
	logger        basic.Logger
	errorsFactory basic.ErrorsFactory
}

func (r *RegistryImpl) Register(methodName string, requestHandler basic.HandlingFunction) {
	r.worker.On(methodName, NewHandler(methodName, requestHandler, r.logger, r.errorsFactory))
}

type CocaineApplication struct {
	appName       string
	service       *cocaineFramework.Service
	serviceSource func() (*cocaineFramework.Service, error)
	logger        basic.Logger
	mutex         sync.Mutex
}

type CocaineAppResult struct {
	result cocaineFramework.ServiceResult
}

func (res *CocaineAppResult) Extract(out interface{}) error {
	var resultChunk struct {
		Value interface{}
	}
	err := res.result.Extract(&resultChunk)
	if err != nil {
		return err
	}
	encoded, ok := resultChunk.Value.([]byte)
	if !ok {
		return fmt.Errorf("Failed to extract encoded part from response.")
	}
	return codec.NewDecoderBytes(encoded, h).Decode(out)
}

func (res *CocaineAppResult) Err() error {
	return res.result.Err()
}

func (app *CocaineApplication) getService() (*cocaineFramework.Service, error) {
	app.mutex.Lock()
	defer func() {
		app.mutex.Unlock()
	}()
	if app.service == nil {
		var err error
		app.service, err = app.serviceSource()
		if err != nil {
			return nil, err
		}
		if app.service == nil {
			return nil, errors.New("Failed to get service. Empty response from cocaine.")
		}
	}
	return app.service, nil
}

func (app *CocaineApplication) Call(serviceMethodName string, args ...interface{}) (basic.ApplicationResult, error) {
	app.logger.Debugf("Cocaine12 Application: calling %s::%s.", app.appName, serviceMethodName)
	if len(args) != 2 {
		app.logger.Errf("Cocaine12 Application: expecting 2 args. Got: %v.", len(args))
		return nil, fmt.Errorf("Wrong number of arguments in call.")
	}
	appMethodName, ok := args[0].(string)
	if !ok {
		return nil, fmt.Errorf("Wrong first argument type. Expected string. Got: %T", args[0])
	}
	callData, ok := args[1].([]byte)
	if !ok {
		return nil, fmt.Errorf("Wrong first argument type. Expected []byte. Got: %T", args[1])
	}
	service, err := app.getService()
	if err != nil {
		return nil, err
	}
	callContext := context.Background()
	resultChan, err := service.Call(callContext, serviceMethodName, appMethodName)
	if err != nil {
		app.logger.Errf("Cocaine12 Application: failed to initiate calling interface. Error: %v.", err)
		return nil, err
	}
	defer resultChan.Call(callContext, "close")

	err = resultChan.Call(callContext, "write", callData)
	if err != nil {
		app.logger.Errf("Cocaine12 Application: failed to send data. Error: %v.", err)
		return nil, err
	}

	result, err := resultChan.Get(callContext)
	if err != nil {
		app.logger.Errf("Cocaine12 Application: call failed. Error: %v.", err)
		return nil, err
	}
	err = result.Err()
	if err != nil {
		app.logger.Errf("Cocaine12 Application: result contains error: %v.", err)
		return nil, err
	}

	app.logger.Debugf("Cocaine12 Application: Call done.")
	if result == nil {
		app.logger.Debugf("Cocaine12 Application: result is empty.")
		return nil, nil //Is this an error?
	}
	app.logger.Debugf("Cocaine12 Application: result is not empty.")
	return &CocaineAppResult{result: result}, nil
}

type Cocaine12Cloud struct {
	logger basic.Logger
}

func (c *Cocaine12Cloud) GetApp(appName string, args ...interface{}) (app basic.Application, err error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("Wrong arguments number provider. Expected 1, got: %d", len(args))
	}

	endpoint, ok := args[0].(string)
	if !ok {
		return nil, fmt.Errorf("Failed to get end-point from arguments. Expected string, but got '%T'.", args[0])
	}
	endpoints := []string{endpoint}

	serviceSource := func() (service *cocaineFramework.Service, err error) {
		err = errors.New("Failed to get application.")
		service = nil
		defer func() {
			if r := recover(); r != nil {
				c.logger.Debugf("Cocaine11 GetApp %s. Failed: %v.", appName, r)
			}
		}()
		c.logger.Debugf("Cocaine12 GetApp %s", appName)
		service, err = cocaineFramework.NewService(context.Background(), appName, endpoints)

		c.logger.Debugf("Cocaine12 GetApp %s done", appName)
		if err != nil {
			c.logger.Debugf("Cocaine12 GetApp %s. Failed: %v.", appName, err)
			return
		}

		c.logger.Debugf("Cocaine12 GetApp %s. Succeeded.", appName)
		return
	}

	return &CocaineApplication{appName: appName, service: nil, serviceSource: serviceSource, logger: c.logger}, nil
}

func (c Cocaine12Cloud) GetID() int {
	return cloud.CLOUD_COCAINE_12
}

func NewCloud(logger basic.Logger) cloud.Cloud {
	return &Cocaine12Cloud{logger: logger}
}

func RunCocaineApp(appName string, methodRegistration cloud.HandlersRegistration, logger basic.Logger) error {
	w, err := cocaineFramework.NewWorker()
	if err != nil {
		return err
	}

	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s: recovered from: %v", appName, r)
		}
	}()

	w.SetTerminationHandler(func(ctx context.Context) {
		time.Sleep(60 * time.Millisecond)
	})

	w.EnableStackSignal(false)

	registry := &RegistryImpl{worker: w, logger: logger}
	registry.Register("ping", cloud.NewOnPingHandler(appName))
	err = methodRegistration(registry)
	if err != nil {
		return err
	}

	if err = w.Run(nil); err != nil {
		return err
	}
	return nil
}

func NewLogger() (basic.Logger, error) {
	return cocaineFramework.NewLogger(context.Background())
}
