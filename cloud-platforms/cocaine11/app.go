package cocaine11

import (
	"bitbucket.org/coldze/go-sample/basic"
	"bitbucket.org/coldze/go-sample/common"
	"errors"
	cocaineFramework "github.com/cocaine/cocaine-framework-go/cocaine"
	"sync"
)

type RegistryImpl struct {
	worker        *cocaineFramework.Worker
	logger        basic.Logger
	errorsFactory basic.ErrorsFactory
	binds         map[string]cocaineFramework.EventHandler
}

func (r *RegistryImpl) Register(methodName string, requestHandler basic.HandlingFunction) {
	r.binds[methodName] = NewHandler(methodName, requestHandler, r.logger, r.errorsFactory)
}

type CocaineApplication struct {
	appName       string
	service       *cocaineFramework.Service
	serviceSource func() (*cocaineFramework.Service, error)
	logger        basic.Logger
	mutex         sync.Mutex
}

type CocaineAppResult struct {
	result cocaineFramework.ServiceResult
}

func (res *CocaineAppResult) Extract(out interface{}) error {
	return res.result.Extract(out)
}

func (res *CocaineAppResult) Err() error {
	return res.result.Err()
}

func (app *CocaineApplication) getService() (*cocaineFramework.Service, error) {
	app.mutex.Lock()
	defer func() {
		app.mutex.Unlock()
	}()
	if app.service == nil {
		var err error
		app.service, err = app.serviceSource()
		if err != nil {
			return nil, err
		}
		if app.service == nil {
			return nil, errors.New("Failed to get service. Empty response from cocaine.")
		}
	}
	return app.service, nil
}

func (app *CocaineApplication) Call(methodName string, args ...interface{}) (basic.ApplicationResult, error) {
	app.logger.Debugf("Cocaine11 Application: calling %s::%s.", app.appName, methodName)
	service, err := app.getService()
	if err != nil {
		return nil, err
	}
	result := <-service.Call(methodName, args...)
	app.logger.Debugf("Cocaine11 Application: Call done.")
	if result == nil {
		app.logger.Debugf("Cocaine11 Application: result is empty.")
		return nil, nil //Is this an error?
	}
	app.logger.Debugf("Cocaine11 Application: result is not empty.")
	return &CocaineAppResult{result: result}, nil
}

type Cocaine11Cloud struct {
	logger basic.Logger
}

func (c *Cocaine11Cloud) GetApp(appName string, args ...interface{}) (app basic.Application, err error) {

	serviceSource := func() (service *cocaineFramework.Service, err error) {
		err = errors.New("Failed to get application.")
		service = nil
		defer func() {
			if r := recover(); r != nil {
				c.logger.Debugf("Cocaine11 GetApp %s. Failed: %v.", appName, r)
			}
		}()
		c.logger.Debugf("Cocaine11 GetApp %s", appName)
		service, err = cocaineFramework.NewService(appName, args...)

		c.logger.Debugf("Cocaine11 GetApp %s done", appName)
		if err != nil {
			c.logger.Debugf("Cocaine11 GetApp %s. Failed: %v.", appName, err)
			return
		}

		c.logger.Debugf("Cocaine11 GetApp %s. Succeeded.", appName)
		return
	}

	return &CocaineApplication{appName: appName, service: nil, serviceSource: serviceSource, logger: c.logger}, nil
}

func (c *Cocaine11Cloud) GetID() int {
	return cloud.CLOUD_COCAINE_11
}

func NewCloud(logger basic.Logger) cloud.Cloud {
	return &Cocaine11Cloud{logger: logger}
}

func RunCocaineApp(appName string, methodRegistration cloud.HandlersRegistration, logger basic.Logger) error {
	w, err := cocaineFramework.NewWorker()
	if err != nil {
		return err
	}

	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s: recovered from: %v", appName, r)
		}
	}()

	registry := &RegistryImpl{worker: w, logger: logger}
	registry.binds = make(map[string]cocaineFramework.EventHandler)
	registry.Register("ping", cloud.NewOnPingHandler(appName))
	err = methodRegistration(registry)
	if err != nil {
		return err
	}

	w.Loop(registry.binds)
	return nil
}

func NewLogger() (basic.Logger, error) {
	return cocaineFramework.NewLogger()
}
