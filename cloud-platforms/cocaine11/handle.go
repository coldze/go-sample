package cocaine11

import (
	"bitbucket.org/coldze/go-sample/basic"
	"bitbucket.org/coldze/go-sample/common"
	"fmt"
	cocaineFramework "github.com/cocaine/cocaine-framework-go/cocaine"
	"time"
)

func NewHandler(methodName string, handler basic.HandlingFunction, baseLogger basic.Logger, errorsFactory basic.ErrorsFactory) cocaineFramework.EventHandler {
	logger := cloud.NewPrefixedLogger(fmt.Sprintf("[%s]: ", methodName), baseLogger)
	return func(request *cocaineFramework.Request, response *cocaineFramework.Response) {
		defer response.Close()
		logger.Debugf("Method started.")
		methodStarted := time.Now()
		body := <-request.Read()
		defer func() {
			if r := recover(); r != nil {
				logger.Errf("Recovered after panic. Error: %v.", r)
				internalErr := basic.NewInternalError()
				response.ErrorMsg(internalErr.GetCode(), internalErr.GetMessage())
			}
			logger.Infof("Method is done. Time taken nanoseconds: %s", time.Since(methodStarted))
		}()
		result, err := handler(body, logger)
		if err != nil {
			logger.Errf("Handling failed. Error: %v. Code: %d", err.GetMessage(), err.GetCode())
			response.ErrorMsg(err.GetCode(), err.GetMessage())
			return
		}
		response.Write(result)
	}
}
