package basic

type Application interface {
	Call(methodName string, args ...interface{}) (ApplicationResult, error)
}
