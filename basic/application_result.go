package basic

type ApplicationResult interface {
	Extract(out interface{}) error
	Err() error
}
