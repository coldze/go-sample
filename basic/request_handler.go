package basic

type HandlingFunction func(request []byte, logger Logger) (interface{}, CommonError)
