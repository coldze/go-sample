package basic

const (
	CODE_OK                   int = 200
	ERROR_CODE_START          int = 1000
	error_code_internal_error int = -32605

	TEXT_METHOD_SUCCEEDED     = "succeeded"
	text_error_internal_error = "Internal error."
)

type CommonError interface {
	error
	GetCode() int
	GetMessage() string
}

type ErrorsFactory interface {
	NewError(code int) CommonError
}

type commonErrorImpl struct {
	code    int
	message string
}

func (a *commonErrorImpl) Error() string {
	return a.GetMessage()
}

func (a *commonErrorImpl) GetMessage() string {
	return a.message
}

func (a *commonErrorImpl) GetCode() int {
	return a.code
}

func NewError(code int, message string) CommonError {
	return &commonErrorImpl{code: code, message: message}
}

func NewInternalError() CommonError {
	return &commonErrorImpl{code: error_code_internal_error, message: text_error_internal_error}
}
