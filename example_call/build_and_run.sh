#!/bin/bash
set -e

PROJECT_NAME='example_call'

mkdir -p build
cd build && rm -rf *
go build ..
./example_call
cd ..
