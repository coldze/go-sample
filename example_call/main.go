package main

import (
	"bitbucket.org/coldze/go-sample/cloud-platforms/cocaine12"
	"bitbucket.org/coldze/go-sample/common"
	"fmt"
	"github.com/ugorji/go/codec"
)

type LocalLogger struct {
}

func (l *LocalLogger) Debugf(msg string, args ...interface{}) {
	fmt.Printf("[DEBUG] ")
	fmt.Printf(msg, args...)
	fmt.Println()
}

func (l *LocalLogger) Infof(msg string, args ...interface{}) {
	fmt.Printf("[INFO] ")
	fmt.Printf(msg, args...)
	fmt.Println()
}

func (l *LocalLogger) Warnf(msg string, args ...interface{}) {
	fmt.Printf("[WARNING] ")
	fmt.Printf(msg, args...)
	fmt.Println()
}
func (l *LocalLogger) Errf(msg string, args ...interface{}) {
	fmt.Printf("[ERROR] ")
	fmt.Printf(msg, args...)
	fmt.Println()
}

func (l *LocalLogger) Close() {
}

var (
	mh codec.MsgpackHandle
	h  = &mh
)

func main() {
	var cloudRegistry cloud.CloudRegistry

	logger12 := &LocalLogger{}
	defer logger12.Close()

	cloudRegistry.RegisterCloud(cocaine12.NewCloud(logger12))

	app, err := cloudRegistry.GetApp(cloud.CLOUD_COCAINE_12, "example_ping", "localhost:10053")
	if err != nil {
		fmt.Printf("Failed getting app. Error: %v\n", err)
		return
	}

	resVal, err := app.Call("enqueue", "ping", []byte("ASDKLASDLAKSD"))
	if err != nil {
		fmt.Printf("Failed calling app. Error: %v\n", err)
		return
	}
	var callRes string

	err = resVal.Extract(&callRes)
	if err != nil {
		fmt.Printf("Failed calling app. Error: %v\n", err)
		return
	} else {
		fmt.Printf("Result: %v\n", callRes)
	}
}
