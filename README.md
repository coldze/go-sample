# About #
This repository contains a part of library for developing cloud applications for different platforms, the simplest application for Yandex's cloud platform (cocaine v12) and application, that interacts with the cloud application.

# Purpose #
The main idea of this library was to create a layer of abstraction over as many cloud platforms as possible, to split apart business logic and platform specific logic.

While using this library one can stay focused on creating business logic and be confident that this logic won't be changed even if the target platform for the application will be changed.
This library also provides the ability for applications on different platforms to communicate with each other.

Here one can find implementations for Yandex's cloud platforms cocaine v11 and cocaine v12. The main difference between those platforms is approach in communication with applications.

# Contents #
* Package 'basic' contains interfaces that are abstractions over cloud platforms.
* Package 'common' contains some implementations that are common for all platforms.
* Package 'cocaine-lib' contains implementations for cocaine cloud platforms.
* Small example of cocaine application can be found in 'example_ping'.
* Example of calling application can be found in 'example_call'.

# Running the code #
I use docker containers for developing and running applications.
One can find prepared docker image for running those sample applications [here](https://hub.docker.com/r/coldze/cocaine12_sample/). It contains already cloud platform (cocaine v12), go 1.5.1 (Yandex's cocaine-framework-go fails to compile with go 1.6, so I have to use 1.5.1)
To run applications with the help of docker images follow the instructions:
```
#!bash

docker pull coldze/cocaine12_sample:latest #pulls docker image from docker-hub
docker run -d --privileged=true -p 352:22 --name=go_sample coldze/cocaine12_sample #runs docker image and gives a name to container == go_sample, maps 22 port to 352 on localhost
ssh root@localhost -p 352 #credentials root:root. Connecting through ssh to the container

#now You're inside the container through ssh
go get github.com/ugorji/go/codec
go get bitbucket.org/coldze/go-sample
cd $GOPATH/src/bitbucket.org/coldze/go-sample/example_ping
./build_and_upload.sh #builds cloud application and uploads into cocaine runtime
cd ../example_call
./build_and_run.sh #builds and runs application that is able to communicate with previous application
exit
```