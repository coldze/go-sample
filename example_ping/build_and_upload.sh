#!/bin/bash
set -e

PROJECT_NAME='example_ping'

mkdir -p build
cd build && rm -rf *
go build ..
cd ..
cocaine-tool app stop --name $PROJECT_NAME || true
cocaine-tool app remove --name $PROJECT_NAME || true
rm -f /var/cache/cocaine/manifests/$PROJECT_NAME || true
rm -f build/PROJECT_NAME.tar.gz || true
cd build
tar -czf $PROJECT_NAME.tar.gz $PROJECT_NAME
cd ..
cocaine-tool profile upload --profile=cocaine/default.profile --name=default
mkdir -p /var/spool/cocaine/$PROJECT_NAME
cp -f build/$PROJECT_NAME /var/spool/cocaine/$PROJECT_NAME/$PROJECT_NAME
cocaine-tool app upload --name $PROJECT_NAME --package build/$PROJECT_NAME.tar.gz --manifest cocaine/manifest.json
cocaine-tool app start --name $PROJECT_NAME --profile default
python cocaine/call.py