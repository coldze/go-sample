package main

import (
	"bitbucket.org/coldze/go-sample/basic"
	"bitbucket.org/coldze/go-sample/cloud-platforms/cocaine12"
	"bitbucket.org/coldze/go-sample/common"
	"fmt"
)

const (
	APP_NAME = "example_ping"
)

func RegisterHandlers(registry cloud.Registry, cloudRegistry cloud.CloudRegistry, logger basic.Logger) error {
	return nil
}

func main() {
	var cloudRegistry cloud.CloudRegistry

	logger12, err := cocaine12.NewLogger()
	if err != nil {
		fmt.Printf("Failed creating logger for cocaine-12. Error: %v", err)
		return
	}
	defer logger12.Close()

	cloudRegistry.RegisterCloud(cocaine12.NewCloud(logger12))

	regHandlers := func(registry cloud.Registry) error {
		return RegisterHandlers(registry, cloudRegistry, logger12)
	}

	err = cocaine12.RunCocaineApp(APP_NAME, regHandlers, logger12)
	if err != nil {
		fmt.Printf("%v", err)
	}
}
