#!/usr/bin/env python

from cocaine.services import Service

from tornado.gen import coroutine
from tornado.ioloop import IOLoop

service = Service('example_ping')

@coroutine
def main():
    channel = yield service.enqueue('ping')
    yield channel.tx.write('---*** PING DATA ***---')
    yield channel.tx.close()
    chunk = yield channel.rx.get()
    print('Response received - {0}'.format(chunk))

if __name__ == '__main__':
    IOLoop.current().run_sync(main, timeout=999999)